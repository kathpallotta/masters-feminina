//------------------------------------
// Typewriter effect
//------------------------------------
let i = 0
let speed = 50
const txt = 'Confira as últimas notícias'
const objects = document.querySelectorAll('.start-on-scroll');

objObserver = new IntersectionObserver(entries => {
    entries.forEach(entry => {
        if (entry.isIntersecting == true) {
            typeWriter()
        } else {
            i = 0
            document.querySelector(".typewriter").innerHTML = ''
        }
    })
})

objects.forEach(item => {
    objObserver.observe(item);
})

function typeWriter() {
    if (i < txt.length) {
        document.querySelector(".typewriter").innerHTML += txt.charAt(i);
        i++;
        setTimeout(typeWriter, speed);
    }
}

//------------------------------------
// News block animation on mouse over
//------------------------------------
const newsMainItem = document.querySelector('.main-featured-news-block')
const newsItem2 = document.querySelector('#featured-news-2')
const newsItem3 = document.querySelector('#featured-news-3')

let newsMainTitle = document.querySelector('.main-featured-news-block .news-block-title')
let newsItem2Title = document.querySelector('#featured-news-2 .news-block-title')
let newsItem3Title = document.querySelector('#featured-news-3 .news-block-title')

newsMainItem.addEventListener('mouseover', function () {
    newsMainTitle.setAttribute('style', 'animation: goup 0.8s ease-in forwards;')
})

newsMainItem.addEventListener('mouseleave', function () {
    newsMainTitle.setAttribute('style', 'animation: none;')
})

newsItem2.addEventListener('mouseover', function () {
    newsItem2Title.setAttribute('style', 'animation: goup 0.8s ease-in forwards;')
})

newsItem2.addEventListener('mouseleave', function () {
    newsItem2Title.setAttribute('style', 'animation: none;')
})

newsItem3.addEventListener('mouseover', function () {
    newsItem3Title.setAttribute('style', 'animation: goup 0.8s ease-in forwards;')
})

newsItem3.addEventListener('mouseleave', function () {
    newsItem3Title.setAttribute('style', 'animation: none;')
})