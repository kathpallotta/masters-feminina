//-------------------------------------
// Side menu displayed only on mobile
//-------------------------------------

$(document).ready(function () {
    

    $("#Mobile__navigation-menu-icon").on("click", function () {
        $("#Mobile__navigation-menu-icon").css("opacity", "0")
        $("#Mobile__navigation-side-menu").addClass("enter")
    })
    
    $("#exit").on("click", function () {
        $("#Mobile__navigation-side-menu").removeClass("enter")
        $("#Mobile__navigation-menu-icon").css("opacity", "1")
    })
})