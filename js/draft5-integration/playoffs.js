moment.locale("pt-BR");

(function () {
  const upper1 = document.getElementById("upper-1");
  const upper2 = document.getElementById("upper-2");
  const upper3 = document.getElementById("upper-3");
  const lower1 = document.getElementById("lower-1");
  const lower2 = document.getElementById("lower-2");
  const grandFinals = document.getElementById("grand-finals");

  // Get matches
  axios
    .get(`${DRAFT5_API_BASE_URL}/matches/${UPPER_1_MATCH_ID}`)
    .then((response) => {
      setMatchContent(upper1, response.data.data, null);
    });

  axios
    .get(`${DRAFT5_API_BASE_URL}/matches/${UPPER_2_MATCH_ID}`)
    .then((response) => {
      setMatchContent(upper2, response.data.data, null);
    });

  axios
    .get(`${DRAFT5_API_BASE_URL}/matches/${UPPER_3_MATCH_ID}`)
    .then((response) => {
      setMatchContent(upper3, response.data.data, null);
    });

  axios
    .get(`${DRAFT5_API_BASE_URL}/matches/${LOWER_1_MATCH_ID}`)
    .then((response) => {
      setMatchContent(lower1, response.data.data, null);
    });

  axios
    .get(`${DRAFT5_API_BASE_URL}/matches/${LOWER_2_MATCH_ID}`)
    .then((response) => {
      setMatchContent(lower2, response.data.data, null);
    });

  axios
    .get(`${DRAFT5_API_BASE_URL}/matches/${GRAND_FINAL_MATCH_ID}`)
    .then((response) => {
      setMatchContent(grandFinals, response.data.data, null);
    });

  function setMatchContent(matchElement, matchData, particularity) {
    const matchInfo = matchElement.children[0];
    const matchTeamA = matchElement.children[1];
    const matchTeamB = matchElement.children[2];
    const matchFooter = matchElement.children[3];

    const matchDate = moment(matchData.matchDate * 1000);
    const matchTime = matchDate.format("DD/MM - HH:mm");
    const matchDiff = matchDate.diff(moment());
    const matchCountDown = moment.duration(matchDiff).humanize(true);

    if (matchData.isOver) {
      matchElement.classList.add("playoff-match-finished");
    } else if (matchDiff < 0) {
      matchElement.classList.add("playoff-match-live");
    }

    // match info
    matchInfo.children[0].innerHTML = `${matchTime} - ${
      particularity ? `${particularity} -` : ""
      } MD${matchData.bestOf}`;
    if (matchData.isOver) {
      matchInfo.children[1].innerHTML = "finalizada";
    } else if (matchDiff < 0) {
      matchInfo.children[1].innerHTML = "ao vivo!";
    } else {
      matchInfo.children[1].innerHTML = matchCountDown;
    }

    // team a info
    if (matchData.isOver && matchData.series.scoreA < matchData.series.scoreB) {
      matchTeamA.classList.add("match-team-loser");
    }

    matchTeamA.children[0].setAttribute(
      "src",
      `${DRAFT5_API_BASE_URL}/teams/${matchData.teamA.id}/logo`
    );
    matchTeamA.children[1].innerHTML = matchData.teamA.name;
    matchTeamA.children[2].innerHTML = matchData.series.scoreA;

    // team b info
    if (matchData.isOver && matchData.series.scoreA > matchData.series.scoreB) {
      matchTeamB.classList.add("match-team-loser");
    }

    matchTeamB.children[0].setAttribute(
      "src",
      `${DRAFT5_API_BASE_URL}/teams/${matchData.teamB.id}/logo`
    );
    matchTeamB.children[1].innerHTML = matchData.teamB.name;
    matchTeamB.children[2].innerHTML = matchData.series.scoreB;

    // footer
    const matchUrl = `https://draft5.gg/partida/${matchData.matchId}-${slugify(
      matchData.teamA.name
    )}-vs-${slugify(matchData.teamB.name)}-${slugify(
      matchData.tournament.name
    )}`;
    matchFooter.setAttribute("href", matchUrl);
  }
})();
