(function () {
  const featuredNews1 = document.getElementById("featured-news-1");
  const featuredNews2 = document.getElementById("featured-news-2");
  const featuredNews3 = document.getElementById("featured-news-3");
  const newsList1 = document.getElementById("news-list-1");
  const newsList2 = document.getElementById("news-list-2");
  const newsList3 = document.getElementById("news-list-3");

  const params = {
    from: 1,
    to: moment().format('X'),
    tournament: GC_MASTERS_TOURNAMENT_ID
  };

  axios.get(`${DRAFT5_API_BASE_URL}/news`, { params })
    .then(values => {
      const newsData = values.data.data;

      // News 1
      // ==================
      setFeaturedNewsContent(featuredNews1, newsData[0]);
      setFeaturedNewsContent(featuredNews2, newsData[1]);
      setFeaturedNewsContent(featuredNews3, newsData[2]);

      // setNewsListContent(newsList1, newsData[3]);
      // setNewsListContent(newsList2, newsData[4]);
      // setNewsListContent(newsList3, newsData[5]);
    })

  function setFeaturedNewsContent(newsElement, newsData) {
    const isPlayByPlay = newsData.type === 'playByPlay';

    // set title
    const newsTitle = newsElement.children[0].children[0];
    if (isPlayByPlay) {
      const articleTitle = newsData.article.title || `Acompanhe ao vivo ${newsData.teamA.name} vs ${newsData.teamB.name}`;
      newsTitle.innerHTML = articleTitle;
    } else {
      newsTitle.innerHTML = newsData.postTitle;
    }

    // set bg image
    if (isPlayByPlay) {
      const postImage = newsData.article.image || newsData.tournament.image;
      newsElement.style.backgroundImage = `url(${DRAFT5_STATIC_BASE_URL}/${postImage}`;
    } else {
      newsElement.style.backgroundImage = `url(${DRAFT5_STATIC_BASE_URL}/${newsData.postImage}`;
    }

    // set url
    const url = isPlayByPlay ?
    `${DRAFT5_BASE_URL}/partida/${newsData.matchId}-${slugify(newsData.teamA.name)}-vs-${slugify(newsData.teamB.name)}-${slugify(newsData.tournament.name)}` :
      `${DRAFT5_BASE_URL}/noticia/${newsData.postSlug}`;
    newsElement.setAttribute("href", url);
  }

  function setNewsListContent(newsElement, newsData) {
    if (!newsData) {
      newsElement.remove();
      return;
    };

    const isPlayByPlay = newsData.type === 'playByPlay';

    // set title
    const newsTitle = newsElement.children[1].children[0];
    const newsDate = newsElement.children[1].children[1];
    newsTitle.innerHTML = isPlayByPlay ? newsData.article.title : newsData.postTitle;;
    newsDate.innerHTML = new Date(isPlayByPlay ? newsData.article.postDate*1000 : newsData.postDate).toLocaleDateString("default", {
      day: "2-digit",
      month: "short",
      year: "numeric"
    }).replace("de", "").replace("de", "");

    // set bg image
    const newsImage = newsElement.children[0];
    const postImage = isPlayByPlay ? newsData.article.image : newsData.postImage
    newsImage.style.backgroundImage = `url(${DRAFT5_STATIC_BASE_URL}/${postImage}`;

    // set url
    const url = isPlayByPlay ?
    `${DRAFT5_BASE_URL}/partida/${newsData.matchId}-${slugify(newsData.teamA.name)}-vs-${slugify(newsData.teamB.name)}-${slugify(newsData.tournament.name)}` :
      `${DRAFT5_BASE_URL}/noticia/${newsData.postSlug}`;
    newsElement.setAttribute("href", url);
  }
})();
