(function () {
  // Init slider

  // get matches
  axios
    .get(
      `${DRAFT5_API_BASE_URL}/matches?page=1&amount=250&finished=0&tournament=${GC_MASTERS_TOURNAMENT_ID}`
    )
    .then((response) => {
      const slideMatch = document.getElementById("slide-match");
      const matchSlideContainer = document.getElementById(
        "match-slider-container"
      );

      response.data.data.map((match) => {
        if (match.isOver) return;

        const newSlideMatch = slideMatch.cloneNode(true);

        // remove initial id
        newSlideMatch.setAttribute("id", "");

        // set href
        const url = `${DRAFT5_BASE_URL}/partida/${match.matchId}-${slugify(
          match.teamA.name
        )}-vs-${slugify(match.teamB.name)}-${slugify(match.tournament.name)}`;
        newSlideMatch.setAttribute("href", url);

        // set attributes
        // ==========================
        const matchContent = newSlideMatch.children[0].children[0];

        // update maps
        // ==========================
        const mapString = match.scores.reduce((prevVal, currVal, currIndex) => {
          if (currVal.scoreA === 0 && currVal.scoreB === 0) {
            if (currIndex === match.scores.length - 1) {
              return `${prevVal}${currVal.map}`;
            }

            return `${prevVal}${currVal.map} - `;
          }

          if (currIndex === match.scores.length - 1) {
            return `${prevVal}${currVal.map} (${currVal.scoreA}:${currVal.scoreB})`;
          }

          return `${prevVal}${currVal.map} (${currVal.scoreA}:${currVal.scoreB}), `;
        }, "");

        matchContent.children[2].innerHTML = mapString;

        // update status
        // ==========================
        const matchDate = moment(match.matchDate * 1000);
        const matchTimeString = matchDate.format("HH:mm");
        const matchCountDown = moment
          .duration(matchDate.diff(moment()))
          .humanize(true);

        if (matchDate.diff(moment()) < 0) {
          matchContent.children[1].innerHTML = `<span class="live-circle"></span> Ao Vivo`;
          newSlideMatch.classList.add("slider-match-live");
        } else {
          // newSlideMatch.classList.add('slider-match-live')
          matchContent.children[1].innerHTML = `${matchTimeString} - ${matchCountDown}`;
        }

        // update teams
        // ==========================
        const teamsContainer = matchContent.children[0];
        const teamAFlag = teamsContainer.children[0];
        const teamAName = teamsContainer.children[1];
        const score = teamsContainer.children[2];
        const teamBName = teamsContainer.children[3];
        const teamBFlag = teamsContainer.children[4];

        teamAFlag.setAttribute(
          "src",
          `https://static.draft5.gg/assets/flags/${match.teamA.country}.svg`
        );
        teamAName.innerHTML = match.teamA.name;
        teamBFlag.setAttribute(
          "src",
          `https://static.draft5.gg/assets/flags/${match.teamB.country}.svg`
        );
        teamBName.innerHTML = match.teamB.name;

        score.innerHTML = `${match.series.scoreA} - ${match.series.scoreB}`;

        matchSlideContainer.appendChild(newSlideMatch);
      });

      slideMatch.remove();

      $(".match-slider-container").slick({
        dots: false,
        infinite: false,
        variableWidth: true,
        slidesToScroll: 4,
        swipeToSlide: true,
        touchThreshold: 200,
        prevArrow:
          '<div class="slick-prev"><img src="../assets/keyboard-right-arrow-button.svg" width="10" style="transform: rotate(180deg)"></div>',
        nextArrow:
          '<div class="slick-next"><img src="../assets/keyboard-right-arrow-button.svg" width="10" ></div>',
        responsive: [
          {
            breakpoint: 996,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 360,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              variableWidth: false,
            },
          },
        ],
      });
    });
})();
