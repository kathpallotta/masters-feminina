const DRAFT5_API_BASE_URL = "https://api-v3.draft5.gg";
const DRAFT5_STATIC_BASE_URL = "https://static.draft5.gg";
const DRAFT5_BASE_URL = "https://draft5.gg";


// Tournament
const GC_MASTERS_TOURNAMENT_ID = 656;

//Finals
const UPPER_1_MATCH_ID = 9098;
const UPPER_2_MATCH_ID = 9099;
const UPPER_3_MATCH_ID = 9100;
const LOWER_1_MATCH_ID = 9101;
const LOWER_2_MATCH_ID = 9102;
const GRAND_FINAL_MATCH_ID = 9103;