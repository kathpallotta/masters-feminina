//------------------------------------
// Display sticky footer
//------------------------------------

window.addEventListener('scroll', function (event) {
    let y = window.scrollY
    let sponsors = document.querySelector('.Sponsors')
    if (y > 100) {
        sponsors.classList.remove("zero-opacity")
    } else {
        sponsors.classList.add("zero-opacity")
    }
})
